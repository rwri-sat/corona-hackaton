export const schema = gql`
  type User {
    id: Int!
    email: String!
    name: String
    bio: String!
    project: Project
  }

  type Query {
    users: [User]
    user(id: Int!): User
  }

  input UserInput {
    email: String
    name: String
    bio: String
    project: Int
  }

  type Mutation {
    createUser(input: UserInput!): User
    updateUser(id: Int!, input: UserInput!): User
    deleteUser(id: Int!): User
  }
`

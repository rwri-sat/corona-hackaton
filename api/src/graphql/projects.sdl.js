export const schema = gql`
  type Project {
    id: Int!
    createdAt: DateTime!
    updatedAt: DateTime!
    projectLead: String!
    group: User
    name: String
    description: String
  }

  type Query {
    projects: [Project]
    project(id: Int!): Project
  }

  input ProjectInput {
    updatedAt: DateTime
    projectLead: String
    group: Int
    name: String
    description: String
  }

  type Mutation {
    createProject(input: ProjectInput!): Project
    updateProject(id: Int!, input: ProjectInput!): Project
    deleteProject(id: Int!): Project
  }
`
